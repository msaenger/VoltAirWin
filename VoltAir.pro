#
# Copyright (C) 2014 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Add more folders to ship with the application, here
# TODO: This manner of handling deployment folders needs some love. Sub-projects that want to deploy
# folders must make sure their var names don't collide with these.
assets_folder.source = assets
assets_folder.target = .
qml_folder.source = qml
qml_folder.target = .
qml_scenes.depends = $$PWD/qml/Ui.qml
qml_scenes.commands =
QMAKE_EXTRA_TARGETS += qml_scenes

LIQUIDFUN_DIR = third_party/liquidfun

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH +=
CONFIG += resources_big
CONFIG += c++11
QT += multimedia qml quick opengl
DEFINES += GL_GLEXT_PROTOTYPES

HEADERS += \
    DebugManager.h \
    Game.h \
    LevelProgression.h \
    LevelProgressionList.h \
    PlayerProfile.h \
    QmlConstants.h \
    UiInternal.h \
    inputs/PlayerManager.h \
    logics/AcceleratorLogic.h \
    logics/ActorEmitterLogic.h \
    logics/EmitterLogic.h \
    logics/GameInputLogic.h \
    logics/MagneticAttractorLogic.h \
    logics/MagneticHighlightLogic.h \
    logics/ParticleEmitterLogic.h \
    logics/PickupLogic.h \
    logics/RollingMovementLogic.h \
    logics/WaterBodyLogic.h \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2ChainShape.h \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2CircleShape.h \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2EdgeShape.h \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2PolygonShape.h \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2Shape.h \
    third_party/liquidfun/Box2D/Box2D/Collision/b2BroadPhase.h \
    third_party/liquidfun/Box2D/Box2D/Collision/b2Collision.h \
    third_party/liquidfun/Box2D/Box2D/Collision/b2Distance.h \
    third_party/liquidfun/Box2D/Box2D/Collision/b2DynamicTree.h \
    third_party/liquidfun/Box2D/Box2D/Collision/b2TimeOfImpact.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2BlockAllocator.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2Draw.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2FreeList.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2GrowableBuffer.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2GrowableStack.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2IntrusiveList.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2Math.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2Settings.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2SlabAllocator.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2StackAllocator.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2Stat.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2Timer.h \
    third_party/liquidfun/Box2D/Box2D/Common/b2TrackedBlock.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2ChainAndCircleContact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2ChainAndPolygonContact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2CircleContact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2Contact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2ContactSolver.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2EdgeAndCircleContact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2EdgeAndPolygonContact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2PolygonAndCircleContact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2PolygonContact.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2DistanceJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2FrictionJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2GearJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2Joint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2MotorJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2MouseJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2PrismaticJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2PulleyJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2RevoluteJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2RopeJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2WeldJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2WheelJoint.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2Body.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2ContactManager.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2Fixture.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2Island.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2TimeStep.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2World.h \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2WorldCallbacks.h \
    third_party/liquidfun/Box2D/Box2D/Particle/b2Particle.h \
    third_party/liquidfun/Box2D/Box2D/Particle/b2ParticleAssembly.h \
    third_party/liquidfun/Box2D/Box2D/Particle/b2ParticleGroup.h \
    third_party/liquidfun/Box2D/Box2D/Particle/b2ParticleSystem.h \
    third_party/liquidfun/Box2D/Box2D/Particle/b2StackQueue.h \
    third_party/liquidfun/Box2D/Box2D/Particle/b2VoronoiDiagram.h \
    third_party/liquidfun/Box2D/Box2D/Rope/b2Rope.h \
    third_party/liquidfun/Box2D/Box2D/Box2D.h

INCLUDEPATH += \
    $$LIQUIDFUN_DIR/Box2D \

SOURCES += \
    DebugManager.cpp \
    Game.cpp \
    LevelProgression.cpp \
    LevelProgressionList.cpp \
    PlayerProfile.cpp \
    UiInternal.cpp \
    inputs/PlayerManager.cpp \
    logics/AcceleratorLogic.cpp \
    logics/ActorEmitterLogic.cpp \
    logics/EmitterLogic.cpp \
    logics/GameInputLogic.cpp \
    logics/MagneticAttractorLogic.cpp \
    logics/MagneticHighlightLogic.cpp \
    logics/ParticleEmitterLogic.cpp \
    logics/PickupLogic.cpp \
    logics/RollingMovementLogic.cpp \
    logics/WaterBodyLogic.cpp \
    main.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2ChainShape.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2CircleShape.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2EdgeShape.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/Shapes/b2PolygonShape.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2BroadPhase.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2CollideCircle.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2CollideEdge.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2CollidePolygon.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2Collision.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2Distance.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2DynamicTree.cpp \
    third_party/liquidfun/Box2D/Box2D/Collision/b2TimeOfImpact.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2BlockAllocator.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2Draw.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2FreeList.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2Math.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2Settings.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2StackAllocator.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2Stat.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2Timer.cpp \
    third_party/liquidfun/Box2D/Box2D/Common/b2TrackedBlock.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2ChainAndCircleContact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2ChainAndPolygonContact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2CircleContact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2Contact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2ContactSolver.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2EdgeAndCircleContact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2EdgeAndPolygonContact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2PolygonAndCircleContact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Contacts/b2PolygonContact.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2DistanceJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2FrictionJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2GearJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2Joint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2MotorJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2MouseJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2PrismaticJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2PulleyJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2RevoluteJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2RopeJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2WeldJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/Joints/b2WheelJoint.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2Body.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2ContactManager.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2Fixture.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2Island.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2World.cpp \
    third_party/liquidfun/Box2D/Box2D/Dynamics/b2WorldCallbacks.cpp \
    third_party/liquidfun/Box2D/Box2D/Particle/b2Particle.cpp \
    third_party/liquidfun/Box2D/Box2D/Particle/b2ParticleAssembly.cpp \
    third_party/liquidfun/Box2D/Box2D/Particle/b2ParticleGroup.cpp \
    third_party/liquidfun/Box2D/Box2D/Particle/b2ParticleSystem.cpp \
    third_party/liquidfun/Box2D/Box2D/Particle/b2VoronoiDiagram.cpp \
    third_party/liquidfun/Box2D/Box2D/Rope/b2Rope.cpp

include(Engine/Engine.pri)
include(GameInput/GameInput.pri)
include(renderer/Renderer.pri)


cache()

LIBS += -lopengl32


RESOURCES += \
    voltair.qrc

DISTFILES += \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/liquidfun-logo.ai \
    third_party/liquidfun/Box2D/Box2D/Documentation/Readme/jquery.js \
    third_party/liquidfun/Box2D/Box2D/Documentation/ReleaseNotes/jquery.js \
    third_party/liquidfun/Box2D/Box2D/Documentation/SWIG/jquery.js \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_18.gif \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_19.gif \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_2.gif \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_20.gif \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_21.gif \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_22.gif \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_3.gif \
    third_party/liquidfun/Box2D/Box2D/Documentation/SWIG/liquidfunpaint.jpg \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_0.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_1.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_10.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_11.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_12.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_13.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_14.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_15.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_16.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_17.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_23.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_24.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_4.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_5.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_6.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_7.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_8.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/image_9.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/liquidfun-logo-horizontal-small.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/liquidfun-logo-small.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/liquidfun-logo-square-small.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/liquidfun-logo.png \
    third_party/liquidfun/Box2D/Box2D/Documentation/footer.html \
    third_party/liquidfun/Box2D/Box2D/Documentation/API-Ref/doxyfile \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/doxyfile \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/doxyfile \
    third_party/liquidfun/Box2D/Box2D/Documentation/Readme/doxyfile \
    third_party/liquidfun/Box2D/Box2D/Documentation/ReleaseNotes/doxyfile \
    third_party/liquidfun/Box2D/Box2D/Documentation/SWIG/doxyfile \
    third_party/liquidfun/Box2D/Box2D/Particle/b2ParticleAssembly.neon.s \
    third_party/liquidfun/Box2D/Box2D/Box2DConfig.cmake.in \
    third_party/liquidfun/Box2D/Box2D/UseBox2D.cmake \
    third_party/liquidfun/Box2D/Box2D/CMakeLists.txt \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/Building.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/BuildingAndroid.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/BuildingiOS.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/BuildingJavaScript.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/BuildingLinux.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/BuildingOSX.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/BuildingWindows.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Building/PortingFromBox2D.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter01_Introduction.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter02_Hello_Box2D.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter03_Common.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter04_Collision_Module.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter05_Dynamics_Module.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter06_Bodies.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter07_Fixtures.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter08_Joints.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter09_Contacts.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter10_World.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter11_Particles.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter12_Loose_Ends.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter13_Debug_Drawing.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter14_Limitations.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Chapter15_References.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/ContentLicense.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/Programmers-Guide/Logo.md \
    third_party/liquidfun/Box2D/Box2D/Documentation/SWIG/GettingStarted.md
